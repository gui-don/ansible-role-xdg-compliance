Ansible role: XDG compliance
===================

Makes a linux desktop XDG compliant, for the user executing the role:

- Setup environment variable to force softwares to use XDG directories.
- Adds default arguments to some command to force them to use XDG directories._yu

Limitations
------------

* This role does not check whether or not an application is installed before trying to fix the XDG spec, resulting in a lot of environment variables set and potentially uneeded configuration files.

Requirements
------------

N/A

Role Variables
--------------

*

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- hosts: all
  roles:
    - 'ansible-role-xdg-compliance'
```

License
-------

BSD
